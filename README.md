# End Spawn Datapack
A datapack for Minecraft 1.17 for my End Survival YouTube series that allows you to spawn in the End dimension.

## Installation Help When Creating a New World
1. Download the datapack from my website: https://binaryvigilante.com/downloads/end-spawn-datapack/
2. When creating a new world, press the "Datapacks" button
3. Click "Open Folder"
4. Now drag and drop the zip file you downloaded into the folder that you just opened
5. Go back to Minecraft
6. Click the arrow pointing to the right when hovering over the datapack so the datapack goes to the right column
7. Click "Done"
8. Create your world (you **don't** need to enable cheats!)
9. You are good to go :)